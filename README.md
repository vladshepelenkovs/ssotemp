Getting started with SSOServiceProviderBundle
=============================================

## Installation

Installation is a quick 6 steps process:

1. Set up SSH key
2. Download SSOServiceProviderBundle
3. Enable the Bundle
4. Add routes
5. Configure your application's security.yml
6. Configure the SSOServiceProviderBundle

###1) Set up SSH key

    - run ssh-keygen
    - run eval `ssh-agent`
    - run ssh-add ~/.ssh/<private_key_file> (default - id_rsa)
    - run cat ~/.ssh/<public_key_file> (default - id_rsa.pub) and copy it
    - add the public key to your Bitbucket settings (choose Bitbucket settings from your avatar in the lower left)
    
###2) Download SSOServiceProviderBundle

Modify composer.json
``` json
{
    ...
    "require": {
        ...
        "moneyfge/sso-service-provider": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:moneyfgee/ssoserviceproviderbundle.git"
        }
    ],
    ...
}
```

Run ```composer update```  

###3) Enable bundle
``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = [
        // ...
        new SSO\ServiceProviderBundle\SSOServiceProviderBundle(),
    ];
}
```

###4) Add routes

In app/config/routing.yml:

``` yaml
    sso:
        resource: "@SSOServiceProviderBundle/Resources/config/routing.xml"
        host: "%sso_service_provider.identity_provider_host%"
    
    sso_admin:
        resource: "@SSOServiceProviderBundle/Resources/config/admin_routing.xml"
        host: "%sso_service_provider.identity_provider_host%"
```
    
###6) Configure your application's security.yml

In app/config/security.yml:

``` yaml    
    security: 
        encoders:
            SSO\ServiceProviderBundle\Security\User\SSOUser: plaintext
            
        role_hierarchy:
                ROLE_USER:        ROLE_USER
                ROLE_ADMIN:       ROLE_USER
                ROLE_VERIFICATION_ADMIN: ROLE_USER
                ROLE_SUPER_ADMIN: [ROLE_VERIFICATION_ADMIN, ROLE_ADMIN]
        
        providers:
            sso_user_provider:
                id: sso_service_provider.user_provider
```                
    
For api access:
   
```    
    security:
        firewalls:
            ...
            api:
                pattern: ^/api
                stateless: true                            # Do no set session cookies
                sso_access_token:
                    check_path: /api/check_path
                anonymous: true                           # Anonymous access is not allowed
```

For twig-based application access:

```    
    security:
        firewalls:
            ...
            main:
                pattern: ^/
                sso_authorization_code: true
                anonymous:    true
                logout:
                    success_handler: sso_service_provider.security.logout.success_handler
```

Both firewalls can be added also. 

###6) Configure the SSOServiceProviderBundle
    
In app/config/config.yml:

``` yaml
    sso_service_provider:
        identity_provider_host: "%sso_identity_provider_host%"      # sso.bitinterpay.com
        identity_provider_schema: "%sso_identity_provider_schema%"  # https
        authorization_path: /oauth/v2/auth                          # authozization url (authorize by user session on identity provider)
        token_path: /oauth/v2/token                                 # url for getting tokens
        user_path: /api/user                                        # url for fetching user
        redirect_after_logout: index                                # user will be redirected to this route after logout
        use_refresh_token: true                                     # use refresh token to renew SSOToken (only for sso_authorization_code)
        validate_ssl: "%sso_validate_ssl%"                          # boolean (not used for now)
        oauth_client_id: "%sso_oauth_client_id%"                    # OAuth2 Client ID
        oauth_secret: "%sso_oauth_secret%"                          # OAuth2 secret
        cache_token: "%sso_cache_token%"                            # cache user token in seconds. Recommended 0
        server_secret: "%sso_server_secret%"                        # should be strong secret phrase and equal on all service providers
```
              
