<?php

namespace SSO\ServiceProviderBundle\Security\EntryPoint;


use SSO\ServiceProviderBundle\Service\SSOService;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SSOAuthorizationCodeEntryPoint implements AuthenticationEntryPointInterface
{

    /**
     * @var SSOService
     */
    protected $ssoService;

    /**
     * SSOAuthorizationCodeEntryPoint constructor.
     * @param SSOService $ssoService
     *
     */
    public function __construct(SSOService $ssoService)
    {
        $this->ssoService = $ssoService;
    }
    /**
     * Starts the authentication scheme.
     *
     * @param Request                 $request       The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return RedirectResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $redirectUri = $request->getUri();
        $authorizationUrl = $this->ssoService->prepareAuthorizationUrl($redirectUri);

        return new RedirectResponse($authorizationUrl);
    }
}
