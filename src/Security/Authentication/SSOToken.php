<?php

namespace SSO\ServiceProviderBundle\Security\Authentication;


use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class SSOToken extends AbstractToken
{
    /**
     * @var string
     */
    protected $accessToken;

    /**
     * @var string
     */
    protected $refreshToken;

    /**
     * @var string
     */
    protected $expiresAt;

    public function __construct(array $roles = array())
    {
        parent::__construct($roles);
        // If the user has roles, consider it authenticated
        $this->setAuthenticated(count($roles) > 0);
    }

    public function getCredentials()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return SSOToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     * @return SSOToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @param \DateTime $expiresAt
     * @return SSOToken
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;
        return $this;
    }

    /**
     * @param $expiresIn
     * @return SSOToken
     */
    public function setExpiresIn($expiresIn)
    {
        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        $now->modify('+' . $expiresIn . ' seconds');
        $this->expiresAt = $now;

        return $this;
    }

    /**
     * @return int|string
     */
    public function getExpiresIn()
    {
        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        $interval = $now->diff($this->expiresAt);
        $difference = $interval->format('s');

        return ($difference < 0) ? 0 : $difference;
    }

    public function isExpired()
    {
        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        return $this->getExpiresAt() < $now;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(array($this->accessToken, $this->refreshToken, $this->expiresAt, parent::serialize()));
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list($this->accessToken, $this->refreshToken, $this->expiresAt, $parentStr) = unserialize($serialized);
        parent::unserialize($parentStr);
    }

}
