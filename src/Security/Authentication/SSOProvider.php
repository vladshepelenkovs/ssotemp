<?php

namespace SSO\ServiceProviderBundle\Security\Authentication;

use SSO\ServiceProviderBundle\Security\User\SSOUserProvider;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class SSOProvider implements AuthenticationProviderInterface
{
    /**
     * @var SSOUserProvider
     */
    protected $userProvider;

    public function __construct(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    /**
     * @param TokenInterface|SSOToken $token
     * @return SSOToken|TokenInterface
     */
    public function authenticate(TokenInterface $token)
    {
        try {
            $user = $this->userProvider->loadUserByAccessToken($token->getAccessToken());
            $authenticatedToken = new SSOToken($user->getRoles());
            $authenticatedToken->setAccessToken($token->getAccessToken());
            $authenticatedToken->setRefreshToken($token->getRefreshToken());
            $authenticatedToken->setExpiresAt($token->getExpiresAt());
            $authenticatedToken->setUser($user);

            return $authenticatedToken;
        }
        catch (\Exception $e)
        {
            throw new AuthenticationException($e->getMessage());
        }
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof SSOToken;
    }
}
