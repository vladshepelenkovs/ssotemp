<?php

namespace SSO\ServiceProviderBundle\Security\User;

use Predis\Client;
use SSO\ServiceProviderBundle\Service\SSOHttpClientService;
use SSO\ServiceProviderBundle\Service\SSOService;
use SSO\ServiceProviderBundle\Traits\IsValidUserArray;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

class SSOUserProvider implements UserProviderInterface
{
    use IsValidUserArray;

    const REDIS_PREFIX = 'token.';

    /**
     * @var Client
     */
    protected $redis;

    /**
     * @var SSOHttpClientService
     */
    protected $ssoHttpClientService;

    /**
     * @var bool
     */
    protected $cacheToken;

    /**
     * SSOUserProvider constructor.
     * @param Client $redis
     * @param SSOHttpClientService $ssoHttpClientService
     * @param $cacheToken
     */
    public function __construct(Client $redis, SSOHttpClientService $ssoHttpClientService, $cacheToken)
    {
        $this->redis = $redis;
        $this->ssoHttpClientService = $ssoHttpClientService;
        $this->cacheToken = $cacheToken;
    }

    public function loadUserByUsername($accessToken)
    {
        return $this->loadUserByAccessToken($accessToken);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof SSOUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        $accessToken = $user->getAccessToken();

        return $this->loadUserByAccessToken($accessToken);
    }

    public function loadUserByAccessToken($accessToken)
    {
        $userArray = $this->fetchUser($accessToken);

        if ($this->isValidUserArray($userArray)) {
            $userArray['accessToken'] = $accessToken;

            return new SSOUser($userArray);
        }

        throw new UsernameNotFoundException(
            sprintf('Error fetching user.')
        );
    }

    public function supportsClass($class)
    {
        return SSOUser::class === $class;
    }

    public function fetchUser(string $accessToken)
    {
        $redisKey = $this->getRedisTokenKey($accessToken);

        if ($this->cacheToken && $json = $this->redis->get($redisKey)) {
            return $this->decode($json);
        }

        $accessToken = SSOService::TOKEN_PREFIX . $accessToken;
        $json = $this->ssoHttpClientService->fetchUserByAccessToken($accessToken);

        if (!$json) {
            return false;
        }

        if ($this->cacheToken) {
            $this->redis->set($redisKey, $json, 'ex', $this->cacheToken);
        }

        return $this->decode($json);
    }

    protected function decode($json)
    {
        try {
            return (new JsonDecode(true))->decode($json, JsonEncoder::FORMAT);
        } catch (NotEncodableValueException $e) {
            return null;
        }
    }

    protected function getRedisTokenKey($apiKey)
    {
        return self::REDIS_PREFIX . md5($apiKey);
    }

}
