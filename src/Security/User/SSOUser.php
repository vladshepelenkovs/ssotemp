<?php

namespace SSO\ServiceProviderBundle\Security\User;

use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class SSOUser implements UserInterface, EquatableInterface
{
    const ROLE_DEFAULT = 'ROLE_USER';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    const STATUS_CONFIRMED = 1;
    const STATUS_WAIT_CONFIRM = 2;
    const STATUS_NOT_CONFIRM = 3;
    const STATUS_CANCELED = 4;

    public static $statuses = [
        self::STATUS_CONFIRMED => 'Verificated',
        self::STATUS_WAIT_CONFIRM => 'Verification on moderation',
        self::STATUS_NOT_CONFIRM => 'No verificated yet',
        self::STATUS_CANCELED => 'Verification canceled',
    ];

    public static $roleList = [
        self::ROLE_DEFAULT => 'ROLE_USER',
        self::ROLE_SUPER_ADMIN => 'ROLE_SUPER_ADMIN',
    ];

    protected $id;

    protected $username;

    protected $avatar;

    protected $status = 1;

    protected $roles;

    protected $accessToken;

    public function __construct(array $userArray)
    {
        $this->id = $userArray['id'];
        $this->username = $userArray['username'];
        $this->roles = $userArray['roles'];

        if ($userArray['avatar']) {
            $this->avatar = [
                'small' => $userArray['avatar_small_thumb'],
                'medium' => $userArray['avatar_medium_thumb'],
            ];
        }

        $this->accessToken = $userArray['accessToken'] ?? null;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

    public function eraseCredentials()
    {
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRoles()
    {
        $roles = $this->roles;
        $roles[] = self::ROLE_DEFAULT;

        return array_unique($roles);
    }

    public function getPassword()
    {
        return null;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     * @return SSOUser
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        return self::$statuses[$this->getStatus()];
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof SSOUser) {
            return false;
        }

        if ($this->id !== $user->getId()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }


}
