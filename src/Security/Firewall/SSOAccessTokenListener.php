<?php

namespace SSO\ServiceProviderBundle\Security\Firewall;

use SSO\ServiceProviderBundle\Security\Authentication\SSOToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;

class SSOAccessTokenListener implements ListenerInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    /**
     * @var AuthenticationManagerInterface
     */
    protected $authenticationManager;
    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }
    /**
     * {@inheritDoc}
     */
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $authHeader = preg_split('/[\s]+/', $request->headers->get('Authorization'));
        $accessToken = isset($authHeader[1]) ? $authHeader[1] : $request->get('access_token');

        if (!empty($accessToken)) {
            $token = new SSOToken();
            $token->setAccessToken($accessToken);
            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);

            return;
        }

        $response = new Response(null, 401);
        $event->setResponse($response);
    }
}
