<?php

namespace SSO\ServiceProviderBundle\Security\Firewall;

use SSO\ServiceProviderBundle\Security\Authentication\SSOToken;
use SSO\ServiceProviderBundle\Service\SSOService;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Http\Firewall\AbstractAuthenticationListener;
use Symfony\Component\HttpFoundation\Request;

class SSOAuthorizationCodeListener extends AbstractAuthenticationListener
{
    /**
     * @var SSOService
     */
    protected $ssoService;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var bool
     */
    protected $useRefreshToken;


    /**
     * @param SSOService $ssoService
     */
    public function setSSOService(SSOService $ssoService)
    {
        $this->ssoService = $ssoService;
    }

    /**
     * @param TokenStorage $tokenStorage
     */
    public function setTokenStorage(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param bool $useRefreshToken
     */
    public function setUseRefreshToken($useRefreshToken)
    {
        $this->useRefreshToken = $useRefreshToken;
    }

    /**
     * {@inheritDoc}
     */
    public function requiresAuthentication(Request $request)
    {
        return true;
    }
    /**
     * {@inheritDoc}
     */
    protected function attemptAuthentication(Request $request)
    {
        // renew SSOToken if expired using RefreshToken

        if ($this->useRefreshToken) {
            $token = $this->tokenStorage->getToken();

            if ($token instanceof SSOToken && $token->isExpired()) {
                $tokenData = $this->ssoService->authorizeByRefreshToken($token->getRefreshToken());

                return $this->ssoAuthentificate($tokenData);
            }
        }

        // Authorize by Authorization code if exists in request

        if(!$request->query->has('code')
            || $request->getSession()->get('state') != $request->query->get('state')
        ) {
            return null;
        }

        $authCode = $request->query->get('code');
        $tokenData = $this->ssoService->authorizeByAuthCode($authCode);

        return $this->ssoAuthentificate($tokenData);
    }

    /**
     * @param $tokenData
     * @return null|\Symfony\Component\Security\Core\Authentication\Token\TokenInterface
     */
    private function ssoAuthentificate($tokenData)
    {
        if (!is_array($tokenData)) {
            return null;
        }

        $token = new SSOToken();
        $token->setAccessToken($tokenData[SSOService::ACCESS_TOKEN] ?? null);
        $token->setExpiresIn($tokenData[SSOService::EXPIRES_IN] ?? null);
        $token->setRefreshToken($tokenData[SSOService::REFRESH_TOKEN] ?? null);

        return $this->authenticationManager->authenticate($token);
    }
}
