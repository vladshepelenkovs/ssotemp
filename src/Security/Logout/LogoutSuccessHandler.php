<?php

namespace SSO\ServiceProviderBundle\Security\Logout;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler;

class LogoutSuccessHandler extends DefaultLogoutSuccessHandler
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var string
     */
    protected $redirectAfterLogout;

    public function setRouter(Router $router, $redirectAfterLogout)
    {
        $this->router = $router;
        $this->redirectAfterLogout = $redirectAfterLogout;
    }

    public function onLogoutSuccess(Request $request)
    {
        $localUri = $this->router->generate($this->redirectAfterLogout, [], Router::ABSOLUTE_URL);
        $uri = $this->router->generate('sso_logout', ['redirect_uri' => $localUri]);

        return new RedirectResponse($uri);
    }
}
