<?php

namespace SSO\ServiceProviderBundle;

use SSO\ServiceProviderBundle\DependencyInjection\Security\Factory\SSOAccessTokenFactory;
use SSO\ServiceProviderBundle\DependencyInjection\Security\Factory\SSOAuthorizationCodeFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SSOServiceProviderBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');

        $extension->addSecurityListenerFactory(new SSOAuthorizationCodeFactory());
        $extension->addSecurityListenerFactory(new SSOAccessTokenFactory());
    }
}
