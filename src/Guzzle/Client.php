<?php

namespace SSO\ServiceProviderBundle\Guzzle;


use Symfony\Component\HttpFoundation\Request;

class Client extends \GuzzleHttp\Client
{
    /**
     * Client constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $request = Request::createFromGlobals();

        $config['headers']['User-Agent'] = $request->headers->get('User-Agent');
        $config['headers']['X-Client-IP'] = $request->getClientIp();

        parent::__construct($config);
    }
}
