<?php

namespace SSO\ServiceProviderBundle\DependencyInjection;

use SSO\ServiceProviderBundle\Security\Logout\LogoutSuccessHandler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class SSOServiceProviderExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $prefix = $this->getAlias() . '.';
        foreach ($config as $name => $value) {
            $container->setParameter($prefix . $name, $value);
        }

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $definition = $container->getDefinition('sso_service_provider.security.authentication.authorization_code_listener');
        $definition->addMethodCall('setTokenStorage', [
            new Reference('security.token_storage'),
        ]);
    }

    public function getAlias()
    {
        return 'sso_service_provider';
    }

}
