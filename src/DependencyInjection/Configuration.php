<?php

namespace SSO\ServiceProviderBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $treeBuilder->root('sso_service_provider')
            ->children()
                ->scalarNode('identity_provider_host')->isRequired()->end()
                ->scalarNode('identity_provider_schema')->isRequired()->end()
                ->scalarNode('authorization_path')->isRequired()->end()
                ->scalarNode('token_path')->isRequired()->end()
                ->scalarNode('user_path')->isRequired()->end()
                ->scalarNode('redirect_after_logout')->isRequired()->end()
                ->scalarNode('use_refresh_token')->isRequired()->end()
                ->scalarNode('validate_ssl')->isRequired()->end()
                ->scalarNode('oauth_client_id')->isRequired()->end()
                ->scalarNode('oauth_secret')->isRequired()->end()
                ->scalarNode('cache_token')->isRequired()->end()
                ->scalarNode('server_secret')->isRequired()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
