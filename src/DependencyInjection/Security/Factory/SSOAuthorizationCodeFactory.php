<?php

namespace SSO\ServiceProviderBundle\DependencyInjection\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

class SSOAuthorizationCodeFactory extends AbstractFactory
{
    public function getPosition()
    {
        return 'http';
    }

    public function getKey()
    {
        return 'sso_authorization_code';
    }

    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $providerId = 'security.authentication.provider.oauth2.'.$id;
        $container
            ->setDefinition($providerId, new ChildDefinition('sso_service_provider.security.authentication.provider'))
            ->replaceArgument(0, new Reference($userProviderId))
        ;

        return $providerId;
    }

    protected function getListenerId()
    {
        return 'sso_service_provider.security.authentication.authorization_code_listener';
    }

    protected function createListener($container, $id, $config, $userProvider)
    {
        $listenerId = parent::createListener($container, $id, $config, $userProvider);
        $container->getDefinition($listenerId);

        return $listenerId;
    }

    protected function createEntryPoint($container, $id, $config, $defaultEntryPoint)
    {
        $entryPointId = 'security.authentication.entry_point.oauth2.'.$id;
        $container
            ->setDefinition($entryPointId, new ChildDefinition('sso_service_provider.security.entry_point.authorization_code_entry_point'))
        ;

        return $entryPointId;
    }
}
