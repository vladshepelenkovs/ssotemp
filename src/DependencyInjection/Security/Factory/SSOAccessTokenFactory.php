<?php

namespace SSO\ServiceProviderBundle\DependencyInjection\Security\Factory;


use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;

class SSOAccessTokenFactory extends AbstractFactory
{
    public function getPosition()
    {
        return 'http';
    }

    public function getKey()
    {
        return 'sso_access_token';
    }

    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $providerId = 'security.authentication.provider.oauth2.'.$id;
        $container
            ->setDefinition($providerId, new ChildDefinition('sso_service_provider.security.authentication.provider'))
            ->replaceArgument(0, new Reference($userProviderId));
        ;

        return $providerId;
    }

    protected function getListenerId()
    {
        return 'sso_service_provider.security.authentication.access_token_listener';
    }

    protected function createListener($container, $id, $config, $userProvider)
    {
        $listenerId = 'security.authentication.listener.oauth2.'.$id;
        $container
            ->setDefinition($listenerId, new ChildDefinition('sso_service_provider.security.authentication.access_token_listener'));

        return $listenerId;
    }

    protected function createEntryPoint($container, $id, $config, $defaultEntryPoint)
    {
        $entryPointId = 'security.authentication.entry_point.oauth2.'.$id;
        $container
            ->setDefinition($entryPointId, new ChildDefinition('sso_service_provider.security.entry_point.access_token_entry_point'));

        return $entryPointId;
    }
}
