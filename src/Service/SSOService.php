<?php

namespace SSO\ServiceProviderBundle\Service;


use SSO\ServiceProviderBundle\Security\User\SSOUser;
use SSO\ServiceProviderBundle\Traits\IsValidUserArray;
use Symfony\Component\HttpFoundation\Session\Session;

class SSOService
{
    use IsValidUserArray;

    const ACCESS_TOKEN = 'access_token';
    const AUTHORIZATION_CODE = 'authorization_code';
    const REFRESH_TOKEN = 'refresh_token';
    const EXPIRES_IN = 'expires_in';

    const TOKEN_PREFIX = 'Bearer ';
    const REDIRECT_URI = 'redirect_uri';

    /**
     * @var SSOHttpClientService
     */
    protected $ssoHttpClientService;

    /**
     * SSOService constructor.
     * @param SSOHttpClientService $ssoHttpClientService
     */
    public function __construct(SSOHttpClientService $ssoHttpClientService)
    {
        $this->ssoHttpClientService = $ssoHttpClientService;
    }

    /**
     * @param $authCode
     * @return mixed|null
     */
    public function authorizeByAuthCode($authCode)
    {
        if (!$authCode) {
            return null;
        }

        $authData = $this->ssoHttpClientService->fetchAuthorizationByAuthCode($authCode);

        return $authData;
    }

    /**
     * @param $refreshToken
     * @return mixed|null
     */
    public function authorizeByRefreshToken($refreshToken)
    {
        if (!$refreshToken) {
            return null;
        }

        $authData = $this->ssoHttpClientService->fetchAuthorizationByRefreshToken($refreshToken);

        return $authData;
    }

    /**
     * @param $redirectUri
     * @return string
     */
    public function prepareAuthorizationUrl($redirectUri)
    {
        $state = md5(rand());
        $session = new Session();
        $session->set('state', $state);

        return $this->ssoHttpClientService->getAuthorizationUrl($redirectUri, $state);
    }

    /**
     * Get user by username. Create if not exists.
     *
     * @param $username
     * @return null|SSOUser
     */
    public function provideServerUser($username)
    {
        $userArray = $this->ssoHttpClientService->serverGet(sprintf('/api/server/user/%s/provide', $username));

        if (!$this->isValidUserArray($userArray)) {
            return null;
        }

        return new SSOUser($userArray);
    }

    /**
     * @param $username
     * @return null|SSOUser
     */
    public function getServerUserByUsername($username)
    {
        $userArray = $this->ssoHttpClientService->serverGet(sprintf('/api/server/user/%s/username', $username));

        if (!$this->isValidUserArray($userArray)) {
            return null;
        }

        return new SSOUser($userArray);
    }

    /**
     * @param $id
     * @return null|SSOUser
     */
    public function getServerUserById($id)
    {
        $userArray = $this->ssoHttpClientService->serverGet(sprintf('/api/server/user/%s/id', $id));

        if (!$this->isValidUserArray($userArray)) {
            return null;
        }

        return new SSOUser($userArray);
    }

    /**
     * @param array $ids
     * @return array|SSOUser[]
     */
    public function getServerUsersByIds(array $ids)
    {
        if (empty($ids)) {
            return [];
        }

        $uri = http_build_query(['id' => array_unique($ids)]);
        $usersArray = $this->ssoHttpClientService->serverGet('/api/server/users/ids?' . $uri);

        return $this->createUsersFromArray($usersArray);
    }

    /**
     * @param $username
     * @return array|SSOUser[]
     */
    public function getServerUsersByUsername($username)
    {
        $usersArray = $this->ssoHttpClientService->serverGet(sprintf('/api/server/users/%s/username', $username));

        return $this->createUsersFromArray($usersArray);
    }

    /**
     * @return null|string
     */
    public function getHttpClientError()
    {
        return $this->ssoHttpClientService->getError();
    }

    /**
     * @param $usersArray
     * @return array|SSOUser[]
     */
    protected function createUsersFromArray($usersArray)
    {
        $users = [];

        if (!\is_array($usersArray)) {
            return [];
        }

        foreach ($usersArray as $userArray) {
            if (!$this->isValidUserArray($userArray)) {
                continue;
            }

            $user = new SSOUser($userArray);
            $users[$user->getId()] = $user;
        }

        return $users;
    }

}
