<?php

namespace SSO\ServiceProviderBundle\Service;

use GuzzleHttp\Exception\RequestException;
use SSO\ServiceProviderBundle\Guzzle\Client;

class SSOHttpClientService
{
    const AUTHORIZATION = 'Authorization';

    /**
     * @var string
     */
    protected $identityProviderBase;

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $oauthSecret;

    /**
     * @var string
     */
    protected $serverSecret;

    /**
     * @var string
     */
    protected $authorizationPath;

    /**
     * @var string
     */
    protected $tokenPath;

    /**
     * @var string
     */
    protected $userPath;

    /**
     * @var string
     */
    protected $error;

    /**
     * SSOHttpClientService constructor.
     * @param $identityProviderHost
     * @param $identityProviderSchema
     * @param $authorizationPath
     * @param $tokenPath
     * @param $userPath
     * @param $clientId
     * @param $oauthSecret
     * @param $serverSecret
     */
    public function __construct($identityProviderHost,
                                $identityProviderSchema,
                                $authorizationPath,
                                $tokenPath,
                                $userPath,
                                $clientId,
                                $oauthSecret,
                                $serverSecret)
    {
        $this->identityProviderBase = $identityProviderSchema . '://' . $identityProviderHost;
        $this->authorizationPath = $authorizationPath;
        $this->tokenPath = $tokenPath;
        $this->userPath = $userPath;
        $this->clientId = $clientId;
        $this->oauthSecret = $oauthSecret;
        $this->serverSecret = $serverSecret;
    }

    public function serverGet(string $url)
    {
        $headers[self::AUTHORIZATION] = $this->serverSecret;
        $client = new \GuzzleHttp\Client(['headers' => $headers]);

        try {
            $response = $client->get($this->identityProviderBase . $url);
        } catch (RequestException $e) {
            $this->setError($e);

            return null;
        }

        return \json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param string $accessToken
     * @return null|string
     */
    public function fetchUserByAccessToken(string $accessToken)
    {
        $headers[self::AUTHORIZATION] = $accessToken;
        $client = new Client(['headers' => $headers]);

        try {
            $response = $client->get($this->identityProviderBase . $this->userPath);
        } catch (RequestException $e) {
            $this->setError($e);

            return null;
        }

        return $response->getBody()->getContents();
    }

    /**
     * @param $authCode
     * @return mixed|null
     */
    public function fetchAuthorizationByAuthCode($authCode)
    {
        $headers[self::AUTHORIZATION] = 'Basic ' . base64_encode($this->clientId . ':' . $this->oauthSecret);
        $client = new Client(['headers' => $headers]);

        $data['grant_type'] = SSOService::AUTHORIZATION_CODE;
        $data['code'] = $authCode;

        try {
            $response = $client->post($this->identityProviderBase . $this->tokenPath, [
                'json' => $data,
            ]);
        } catch (RequestException $e) {
            $this->setError($e);

            return null;
        }

        return \json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param $refreshToken
     * @return mixed|null
     */
    public function fetchAuthorizationByRefreshToken($refreshToken)
    {
        $headers[self::AUTHORIZATION] = 'Basic ' . base64_encode($this->clientId . ':' . $this->oauthSecret);
        $client = new Client(['headers' => $headers]);

        $data['grant_type'] = SSOService::REFRESH_TOKEN;
        $data[SSOService::REFRESH_TOKEN] = $refreshToken;

        try {
            $response = $client->post($this->identityProviderBase . $this->tokenPath, [
                'json' => $data,
            ]);

        } catch (RequestException $e) {
            $this->setError($e);

            return null;
        }

        return \json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param $redirectUri
     * @param $state
     * @return string
     */
    public function getAuthorizationUrl($redirectUri, $state)
    {
        $params = [
            'response_type' => 'code',
            'client_id' => $this->clientId,
            'redirect_uri' => $redirectUri,
            'scope' => '',
            'state' => $state,
        ];

        return $this->identityProviderBase . $this->authorizationPath .'?' . http_build_query($params);
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     * @return SSOHttpClientService
     */
    public function setError(RequestException $error)
    {
        if ($error->hasResponse()) {
            $error = \json_decode($error->getResponse()->getBody()->getContents());
        } else {
            $error = 'Unknown error';
        }
        $this->error = $error;

        return $this;
    }

}
