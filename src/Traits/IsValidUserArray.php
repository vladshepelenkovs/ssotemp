<?php

namespace SSO\ServiceProviderBundle\Traits;


trait IsValidUserArray
{
    protected function isValidUserArray($userArray)
    {
        if (!is_array($userArray)) {
            return false;
        }
        $properties = ['id', 'username', 'roles', 'avatar'];

        foreach ($properties as $property) {
            if (!array_key_exists($property, $userArray)) {
                return false;
            }
        }

        return true;
    }
}
